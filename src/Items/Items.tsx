import useKeyBind from '@zanchi/use-key-bind'
import compose from 'compose-function'
import { matchSorter } from 'match-sorter'
import { useEffect, useRef, useState } from 'preact/hooks'

import { getInputVal } from 'src/Creatures/utils'
import { ListItem, SearchHeader, useExpandableList } from 'src/components'
import listStyles from 'src/components/ExpandableList/ExpandableList.module.scss'
import tabStyles from 'src/components/TabbedContainer.module.scss'
import { useBool, useJson } from 'src/hooks'

import CreateItem from './CreateItem'
import ExpandedItem from './ExpandedItem'
import { Item } from './types'

const Items = () => {
  const [items, { add: addItem }] = useJson<Item>('items')
  const [creating, { toggle: toggleCreating }] = useBool(false)
  const [searchTerm, setSearchTerm] = useState('')
  const containerRef = useRef<HTMLElement | null>(null)
  const hasFocus = () =>
    Boolean(containerRef.current?.contains(document.activeElement))

  const filteredItems = matchSorter(items, searchTerm, { keys: ['name'] })

  const [ExpandableList, { collapse, expand, expanded, select, selected }] =
    useExpandableList<Item>()

  useKeyBind(
    ['Enter'],
    () => {
      if (!hasFocus()) return
      if (filteredItems.length > 0) {
        // we're hitting enter from the input
        if (selected == null) {
          expand(0)
          select(0)
        } else if (selected === expanded) {
          collapse()
        } else {
          expand(selected)
        }
      }
    },
    []
  )

  useEffect(() => {
    collapse()
    select(null)
  }, [searchTerm])

  return (
    <section
      aria-labelledby="Items-tab"
      class={tabStyles.tabPanel}
      ref={containerRef}
      role="tabpanel"
    >
      <SearchHeader
        onAdd={toggleCreating}
        onInput={compose(setSearchTerm, getInputVal)}
        searchTerm={searchTerm}
        title="Magic Items"
      />
      {creating && <CreateItem onSave={addItem} />}
      <ExpandableList
        className={listStyles.list}
        hasFocus={hasFocus}
        items={filteredItems}
        ExpandedItem={({ onCollapse, ...item }) => (
          <ExpandedItem onCollapse={onCollapse} {...item} />
        )}
        ListItem={({ onExpand, selected, ...item }) => (
          <ListItem
            key={item.name}
            onClick={onExpand}
            subText={item.type}
            selected={selected}
            title={item.name}
            twoLine
          />
        )}
      />
    </section>
  )
}

export default Items
