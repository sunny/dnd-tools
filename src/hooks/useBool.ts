import { useState } from 'preact/hooks'

type Actions = {
  set: (val: boolean) => void
  toggle: () => void
}

/**
 * keep a boolean value in state.
 * exposes two actions, `toggle` and `set`.
 *
 * @example
 * ```
 * const [selected, { set: setSelected, toggle: toggleSelected }] =
 *  useBool(false)
 * ```
 */
const useBool = (initial: boolean): [boolean, Actions] => {
  const [val, set] = useState(initial)
  return [val, { toggle: () => set((v) => !v), set }]
}
export default useBool
