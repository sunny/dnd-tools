import compose from 'compose-function'
import { promises as fs } from 'fs'
import { parse } from 'node-html-parser'

const spellJson = await fs.readFile('./spells_in.json')
const srcSpells = JSON.parse(spellJson)

/**
 * need to have a root element,
 * the source has unwrapped sibling nodes.
 */
const parseHtml = (str = '') => parse(`<div>${str}<div>`)

const renameKey = (key, newKey) => (obj) => {
  if (!(key in obj)) return obj
  const newObj = { ...obj, [newKey]: obj[key] }
  delete newObj[key]
  return newObj
}
const mapProp = (prop, fn) => (obj) => ({
  ...obj,
  [prop]: fn(obj[prop]),
})

const getLevel = (levelStr) => {
  if (levelStr.toLowerCase() === 'cantrip') return 0
  return Number(levelStr.match(/\d/)[0])
}
const splitClasses = mapProp('class', (clss) => clss.split(', '))
const splitComponents = mapProp('components', (comps) => comps.split(', '))
const stripTags = (str) => {
  const html = parseHtml(str)
  return html.innerText
}

const spells = srcSpells.map(
  compose(
    mapProp('level', getLevel),
    mapProp('ritual', (r) => r === 'yes'),
    mapProp('concentration', (c) => c === 'yes'),
    renameKey('casting_time', 'castingTime'),
    renameKey('higher_level', 'higherLevel'),
    mapProp('higher_level', (higherLevel) => {
      if (higherLevel == null) return null
      return stripTags(higherLevel)
    }),
    renameKey('desc', 'description'),
    mapProp('desc', stripTags),
    splitComponents,
    splitClasses
  )
)

fs.writeFile('../data/spells.json', JSON.stringify(spells, null, 2))
