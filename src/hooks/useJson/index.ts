import { useEffect, useState } from 'preact/hooks'
import { append } from 'src/utils'

type Actions<T> = {
  add: (item: T) => void
}

/**
 * loads JSON async to keep the bundle size down.
 */
const useJson = <T>(name: string): [T[], Actions<T>] => {
  const [items, setItems] = useState<T[]>([])

  const add = (item: T) => {
    const fromLS = JSON.parse(localStorage.getItem(name) ?? '[]')
    localStorage.setItem(name, JSON.stringify([...fromLS, item]))
    setItems(append(item))
  }

  useEffect(() => {
    import(`./data/${name}.json`).then(({ default: data }) => {
      const fromLS = JSON.parse(localStorage.getItem(name) ?? '[]')
      setItems([...data, ...fromLS])
    })
  }, [])

  return [items, { add }]
}

export default useJson
