import { useEffect } from 'preact/hooks'

const save = (key: string, data: unknown) => {
  const json = JSON.stringify(data)
  localStorage.setItem(key, json)
}

/**
 * save a value to local storage at the given `key`
 * when the document's `visibilityState` changes to `'hidden'`.
 */
const useSaveOnPageHidden = (key: string, data: unknown) => {
  useEffect(() => {
    const handler = () => {
      const hidden = document.visibilityState === 'hidden'
      if (hidden) {
        save(key, data)
      }
    }

    document.addEventListener('visibilitychange', handler)
    return () => {
      document.removeEventListener('visibilitychange', handler)
    }
  }, [key, data])
}

export default useSaveOnPageHidden
