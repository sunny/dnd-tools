import { useState } from 'preact/hooks'

import { RowValue } from 'src/Initiative/Row'

type TurnActions = {
  nextTurn: () => void
  resetTurnOrder: () => void
}

const useTurn = (rows: RowValue[]): [number | null, TurnActions] => {
  const [turn, setTurn] = useState<number | null>(null)

  const nextTurn = () => {
    if (turn == null) {
      setTurn(0)
      return
    }

    const lastFilledRowIndex =
      rows.length -
      1 -
      [...rows].reverse().findIndex((r) => !Number.isNaN(r.initiative))

    if (turn + 1 > lastFilledRowIndex) {
      setTurn(0)
    } else {
      setTurn(turn + 1)
    }
  }

  const resetTurnOrder = () => setTurn(null)

  return [turn, { nextTurn, resetTurnOrder }]
}

export default useTurn
