import { FunctionalComponent as FC } from 'preact'
import { StateUpdater, useState } from 'preact/hooks'

import ExpandableList, { Props as ELProps } from './ExpandableList'

type Actions = {
  collapse: () => void
  expand: StateUpdater<number | null>
  expanded: number | null
  select: StateUpdater<number | null>
  selected: number | null
}

type HookProps<T> = Pick<
  ELProps<T>,
  'expanded' | 'onExpand' | 'onCollapse' | 'onSelect' | 'selected'
>
type Props<T> = Omit<ELProps<T>, keyof HookProps<T>>

/**
 * this handles the state required to use
 * the ExpandableList component and exposes
 * functions to change the state.
 * it returns the ExpandableList component
 * with the `expanded` and `selected` state,
 * and the update functions.
 */
const useExpandableList: <T>() => [FC<Props<T>>, Actions] = () => {
  const [expanded, setExpanded] = useState<number | null>(null)
  const [selected, setSelected] = useState<number | null>(null)

  const collapse = () => setExpanded(null)

  return [
    <T,>(props: Props<T>) => (
      <ExpandableList
        expanded={expanded}
        onExpand={setExpanded}
        onCollapse={collapse}
        onSelect={setSelected}
        selected={selected}
        {...props}
      />
    ),
    {
      collapse,
      expand: setExpanded,
      expanded,
      select: setSelected,
      selected,
    },
  ]
}

export default useExpandableList
