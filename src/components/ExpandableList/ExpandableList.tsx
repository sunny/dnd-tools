import { FunctionalComponent as FC } from 'preact'
import { StateUpdater, useState } from 'preact/hooks'
import useKeyBind from '@zanchi/use-key-bind'

export type Props<T> = {
  /**
   * allow external styling.
   */
  className?: string
  /**
   * index of the currently expanded item.
   */
  expanded: number | null
  /**
   * the component to render for the currently expanded index.
   * receives the currently expanded list item and a couple other props.
   * you might need an intermediate component to map these props to
   * your component, ie:
   *
   * @example
   * ```jsx
   * <ExpandableItem
   *   ExpandedItem={({ onCollapse, ...creature}) => (
   *     <ExpandedCreature onClick={onCollapse} {...creature} />
   *   )}
   *   ListItem={CreatureListItem}
   * />
   * ```
   */
  ExpandedItem: FC<T & { onCollapse: () => void; selected: boolean }>
  /**
   * this needs to be a function, because it's used in a callback
   * and the focus may have changed since the last time this
   * component was rendered.
   */
  hasFocus: () => boolean
  /**
   * items to render the list for.
   */
  items: T[]
  /**
   * the component to render for each non-expanded list item.
   * receives a list item in addition to a couple other props.
   * you might need an intermediate component to map these props to
   * your component, ie:
   *
   * @example
   * ```jsx
   * <ExpandableItem
   *   ExpandedItem={ExpandedCreature}
   *   ListItem={({ onExpand, ...creature}) => (
   *     <CreatureItem onClick={onExpand} {...creature} />
   *   )}
   * />
   * ```
   */
  ListItem: FC<T & { onExpand: () => void; selected: boolean }>

  onCollapse: () => void
  onExpand: StateUpdater<number | null>
  onSelect: StateUpdater<number | null>
  /**
   * index of the currently selected item.
   */
  selected: number | null
}

const ExpandableList = function <T>({
  className,
  expanded,
  hasFocus,
  ExpandedItem,
  items,
  onCollapse,
  onExpand,
  onSelect,
  selected,
  ListItem,
}: Props<T>) {
  useKeyBind(
    ['ArrowDown'],
    () => {
      if (!hasFocus()) return
      selectNext()
    },
    [onSelect]
  )
  useKeyBind(
    ['ArrowUp'],
    () => {
      if (!hasFocus()) return
      selectPrev()
    },
    [onSelect]
  )

  const expand = (index: number) => () => onExpand(index)

  const selectNext = () => {
    if ((selected ?? 0) + 1 > items.length) return
    onSelect((s) => (s ?? -1) + 1)
  }

  const selectPrev = () => {
    if ((selected ?? 0) - 1 < 0) {
      onSelect(null)
      return
    }
    onSelect((s) => s! - 1)
  }

  return (
    <ol class={className}>
      {items.map((item, i) => {
        if (i === expanded)
          return (
            <ExpandedItem
              {...item}
              // @ts-expect-error we might have a name, who knows,
              // but we can always fall back to using the index.
              key={item.name ?? i}
              onCollapse={onCollapse}
              selected={selected === i}
            />
          )

        return (
          <ListItem
            {...item}
            // @ts-expect-error we might have a name, who knows,
            // but we can always fall back to using the index.
            key={item.name ?? i}
            onExpand={expand(i)}
            onSelect={() => onSelect(i)}
            selected={selected === i}
          />
        )
      })}
    </ol>
  )
}

export default ExpandableList
