import cx from 'classnames'
import { FunctionalComponent as FC } from 'preact'
import styles from './ListItem.module.scss'

type Props = {
  twoLine?: boolean
  onClick: () => unknown
  selected: boolean
  subText: string
  title: string
}

const ListItem: FC<Props> = ({
  twoLine,
  onClick,
  selected,
  subText,
  title,
}) => (
  <li
    class={cx(
      styles.listItem,
      selected && styles.selected,
      twoLine && styles.twoLine
    )}
    onClick={onClick}
  >
    <h2>{title}</h2>
    <p>{subText}</p>
  </li>
)

export default ListItem
