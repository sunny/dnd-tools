import { FunctionalComponent as FC } from 'preact'
import { useState } from 'preact/hooks'

import Creatures from 'src/Creatures'
import { flatHp } from 'src/Creatures/utils'
import { Creature } from 'src/Creatures/types'
import Info from 'src/components/Info'
import TabbedContainer from 'src/components/TabbedContainer'
import { MdProvider } from 'src/hooks/useMdParser'
import { Initiative } from 'src/Initiative'
import { RowValue } from 'src/Initiative/Row'
import Items from 'src/Items'
import Spells from 'src/Spells'
import { useBool, useRows, useSaveOnPageHidden } from 'src/hooks'
import { updateAt } from 'src/utils'

const TABS = [{ name: 'Creatures' }, { name: 'Items' }, { name: 'Spells' }]

const App: FC = () => {
  const [showInfo, { toggle: toggleShowInfo }] = useBool(false)
  const [currentTab, setCurrentTab] = useState(0)
  const [rows, rowActions] = useRows(
    // @ts-expect-error `null` is okay here
    JSON.parse(localStorage.getItem('initiative:rows')) ?? []
  )
  useSaveOnPageHidden('initiative:rows', rows)

  const addCreatureToInitiative = (creature: Creature) => {
    const firstEmptyIndex = rows.findIndex((r) =>
      Object.values(r).every((v) => !v)
    )
    rowActions.set(
      updateAt(firstEmptyIndex, {
        name: creature.name,
        hp: flatHp(creature),
        notes: '',
        initiative: NaN,
      } as RowValue)(rows)
    )
  }

  return (
    <>
      {showInfo && <Info onClose={toggleShowInfo} />}
      <MdProvider>
        <Initiative rows={rows} rowActions={rowActions} />
        <TabbedContainer
          currentTab={currentTab}
          onChange={setCurrentTab}
          onClickInfo={toggleShowInfo}
          tabs={TABS}
        >
          <Creatures onAddToInitiative={addCreatureToInitiative} />
          <Items />
          <Spells />
        </TabbedContainer>
      </MdProvider>
    </>
  )
}

export default App
