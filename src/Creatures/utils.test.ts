import { expect, test } from 'vitest'
import { abilityScoreModifier } from './utils'

test('abilityScoreModifier', () => {
  const abs = abilityScoreModifier
  expect(abs(0)).toEqual(-5)
  expect(abs(10)).toEqual(0)
  expect(abs(20)).toEqual(5)
  expect(abs(13)).toEqual(1)
  expect(abs(14)).toEqual(2)
})
