import creatures from 'src/hooks/useJson/data/creatures.json'

export type AbilityScores = {
  STR: number
  DEX: number
  CON: number
  INT: number
  WIS: number
  CHA: number
}

export type Creature = typeof creatures[number]

export type CreatureSize =
  | 'Tiny'
  | 'Small'
  | 'Medium'
  | 'Large'
  | 'Huge'
  | 'Gargantuan'

export type CreatureType =
  | 'aberration'
  | 'beast'
  | 'celestial'
  | 'dragon'
  | 'elemental'
  | 'fey'
  | 'fiend'
  | 'giant'
  | 'humanoid'
  | 'monstrosity'
  | 'ooze'
  | 'plant'
  | 'undead'

export type Trait = {
  description: string
  /**
   * if the description has additional information
   * in the form of a list, like the spells a spellcaster
   * can cast, each line here will be rendered in a `ul`.
   */
  descriptionList?: string[]
  name: string
}
