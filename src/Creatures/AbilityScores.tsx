import { AbilityScores as AbScores } from './types'
import { abilityScoreModifier as modifier, FULL_ABILITY_NAMES } from './utils'
import styles from './AbilityScores.module.scss'

type Props = {
  editable?: boolean
  onUpdate?: (name: keyof AbScores, newValue: number) => unknown
} & AbScores

/**
 * component to display a creature's ability scores.
 * it can serve as a display or an input, based on
 * the `editable` property.
 * at larger screen sizes,
 * it will display them in a line.
 * at smaller screen sizes,
 * it will display them in a 3x2 grid.
 */
const AbilityScores = ({ editable, onUpdate, ...abilityScores }: Props) => (
  <dl aria-description="ability scores" class={styles.scores}>
    {Object.entries(abilityScores).map(([name, score]) => (
      <div class={styles.score} role="listitem">
        <dt class="visually-hidden">
          {FULL_ABILITY_NAMES[name as keyof AbScores]}
        </dt>
        <dt aria-hidden="true" class={styles.scoreName}>
          {name}
        </dt>
        {!editable && <dd class={styles.scoreValue}>{score}</dd>}
        {editable && onUpdate && (
          <dd>
            <input
              class={styles.scoreInput}
              onInput={(e) =>
                onUpdate(name as keyof AbScores, Number(e.currentTarget.value))
              }
              value={score}
            />
          </dd>
        )}
        <dd class={styles.modifier}>
          ({modifier(score) >= 0 && '+'}
          {modifier(score)})
        </dd>
      </div>
    ))}
  </dl>
)

export default AbilityScores
