import cx from 'classnames'
import empty from 'just-is-empty'
import { FunctionalComponent as FC } from 'preact'

import AbilityScores from './AbilityScores'
import { Creature, Trait } from './types'
import {
  abilityScores,
  alignment,
  physicalTraits,
  shortTraits,
  size,
  type,
} from './utils'
import styles from 'src/components/ExpandedItem.module.scss'

type Props = {
  creature: Creature
  onAdd: () => unknown
  onCollapse: () => unknown
  selected?: boolean
}

export const TraitLine = ({
  description,
  descriptionList = [],
  name,
  spaced,
}: Trait & { spaced?: boolean }) => (
  <div class={cx(spaced && styles.spaced)}>
    <dt>{name} </dt>
    <dd>
      {description}
      {!empty(descriptionList) && (
        <ul>
          {descriptionList.map((item) => (
            <li key={item}>{item}</li>
          ))}
        </ul>
      )}
    </dd>
  </div>
)

const ExpandedCreature: FC<Props> = ({
  creature,
  onAdd,
  onCollapse,
  selected,
}) => {
  const {
    Actions: actions,
    ['Legendary Actions']: legendaryActions,
    name,
    Reactions: reactions,
  } = creature
  const traits = creature.Traits ?? []

  return (
    <li class={cx(styles.expandedItem, { selected })} key={name}>
      <div class={styles.titleGroup} onClick={onCollapse}>
        <h2>{name}</h2>
        <p class={styles.meta}>
          {size(creature)} {type(creature)}, {alignment(creature)}
        </p>
      </div>

      <div class={styles.buttonGroup}>
        <button onClick={onAdd}>add to initiative</button>
      </div>

      <dl>
        {Object.entries(physicalTraits(creature)).map(([name, value]) => (
          <TraitLine description={value} name={name} />
        ))}
      </dl>

      <AbilityScores {...abilityScores(creature)} />

      <dl>
        {Object.entries(shortTraits(creature)).map(([name, value]) => (
          <TraitLine description={value} name={name} />
        ))}
      </dl>

      {!empty(traits) && (
        <>
          <hr />
          <dl>
            {traits.map((trait) => (
              <TraitLine {...trait} spaced />
            ))}
          </dl>
        </>
      )}

      {!empty(actions) && (
        <>
          <hr />
          <section>
            <h3>Actions</h3>
            <dl>
              {actions!.map((action) => (
                <TraitLine {...action} spaced />
              ))}
            </dl>
          </section>
        </>
      )}

      {!empty(reactions) && (
        <>
          <hr />
          <section>
            <h3>Reactions</h3>
            <dl>
              {reactions!.map((action) => (
                <TraitLine {...action} />
              ))}
            </dl>
          </section>
        </>
      )}

      {!empty(legendaryActions?.actions) && (
        <>
          <hr />
          <section>
            <h3>Legendary Actions</h3>
            <p>{legendaryActions!.startText}</p>
            <dl>
              {legendaryActions!.actions.map((la) => (
                <TraitLine {...la} spaced />
              ))}
            </dl>
          </section>
        </>
      )}
    </li>
  )
}

export default ExpandedCreature
