import useKeyBind from '@zanchi/use-key-bind'
import compose from 'compose-function'
import { matchSorter } from 'match-sorter'
import { FunctionComponent as FC } from 'preact'
import { useEffect, useRef, useState } from 'preact/hooks'
import { mapKeys, range } from 'remeda'

import { useBool, useJson } from 'src/hooks'
import { ListItem, SearchHeader, useExpandableList } from 'src/components'
import listStyles from 'src/components/ExpandableList/ExpandableList.module.scss'
import tabStyles from 'src/components/TabbedContainer.module.scss'

import Create from './CreateCreature'
import ExpandedCreature from './ExpandedCreature'
import { Creature } from './types'
import { getInputVal, type } from './utils'

const Loading = () => (
  <div class={listStyles.loading}>
    {range(1, 12).map(() => (
      <div class={listStyles.listItemSkeleton} />
    ))}
  </div>
)

type Props = {
  onAddToInitiative: (c: Creature) => unknown
}

const Creatures: FC<Props> = ({ onAddToInitiative }) => {
  const [creatures, { add: addCreature }] = useJson<Creature>('creatures')
  const [creating, { toggle: toggleCreating }] = useBool(false)
  const [searchTerm, setSearchTerm] = useState('')
  const containerRef = useRef<HTMLElement | null>(null)
  const hasFocus = () =>
    Boolean(containerRef.current?.contains(document.activeElement))

  let filteredCreatures: Creature[]
  if (/<>=/.test(searchTerm)) {
    const operatorIndex = searchTerm.search(/<>=/)
    const key = searchTerm.slice(0, operatorIndex).trim()
    const searchValue = searchTerm.slice(operatorIndex + 1).trim()

    filteredCreatures = creatures.filter((creature) => {
      const lowercaseCreature = mapKeys(creature, (k) =>
        String(k).toLowerCase()
      )

      const value = lowercaseCreature[key as keyof Creature] as string
      return value?.includes(searchValue)
    })
  } else {
    filteredCreatures = matchSorter(creatures, searchTerm, {
      keys: ['name'],
    })
  }

  const [ExpandableList, { collapse, expand, expanded, select, selected }] =
    useExpandableList<Creature>()

  useKeyBind(
    ['Enter'],
    () => {
      if (!hasFocus()) return
      if (filteredCreatures.length > 0) {
        // we're hitting enter from the input
        if (selected == null) {
          expand(0)
          select(0)
        } else if (selected === expanded) {
          collapse()
        } else {
          expand(selected)
        }
      }
    },
    []
  )

  useEffect(() => {
    collapse()
    select(null)
  }, [searchTerm])

  return (
    <section
      aria-labelledby="Creatures-tab"
      class={tabStyles.tabPanel}
      ref={containerRef}
      role="tabpanel"
    >
      <SearchHeader
        onAdd={toggleCreating}
        onInput={compose(setSearchTerm, getInputVal)}
        searchTerm={searchTerm}
        title="Creatures"
      />

      {creating && <Create onSave={addCreature} />}
      {creatures.length === 0 ? (
        <Loading />
      ) : (
        <ExpandableList
          className={listStyles.list}
          hasFocus={hasFocus}
          items={filteredCreatures}
          ExpandedItem={({ onCollapse, selected, ...creature }) => (
            <ExpandedCreature
              creature={creature}
              onAdd={() => onAddToInitiative(creature)}
              onCollapse={onCollapse}
              selected={selected}
            />
          )}
          ListItem={({ onExpand, selected, ...creature }) => (
            <ListItem
              onClick={onExpand}
              selected={selected}
              subText={type(creature)}
              title={creature.name}
            />
          )}
        />
      )}
    </section>
  )
}

export default Creatures
