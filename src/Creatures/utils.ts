import compose from 'compose-function'
import { first, mapValues, pick } from 'remeda'

import {
  AbilityScores as AbScores,
  Creature,
  CreatureSize,
  CreatureType,
} from './types'

/**
 * expects an `Event`,
 * returns `e.currentTarget.value`.
 */
export const getInputVal = (
  e: JSX.TargetedEvent<HTMLInputElement | HTMLTextAreaElement>
) => e.currentTarget.value

export const abilityScores: (creature: Creature) => AbScores = compose(
  mapValues((score) => Number.parseInt(score as string)),
  pick(['STR', 'DEX', 'CON', 'INT', 'WIS', 'CHA'])
)
/**
 * returns the modifier for a given ability score as in the 5e rules.
 * 14 returns 2, 8 returns -1, and so on.
 */
export const abilityScoreModifier = (abilityScore: number): number =>
  Math.floor(abilityScore / 2) - 5
export const alignment = (creature: Creature) => parseMeta(creature).alignment
export const flatHp: (creature: Creature) => number = compose(
  Number,
  first,
  (hpString: string) => hpString.split(' '),
  (creature: Creature) => creature['Hit Points']
)
/**
 * maps ability acronyms to their full names
 */
export const FULL_ABILITY_NAMES: Record<keyof AbScores, string> = {
  STR: 'Strength',
  DEX: 'Dexterity',
  CON: 'Constitution',
  INT: 'Intelligence',
  WIS: 'Wisdom',
  CHA: 'Charisma',
}
export const size = (creature: Creature) => parseMeta(creature).size
export const parseMeta = (
  creature: Creature
): { alignment: string; size: CreatureSize; type: CreatureType } => {
  const [sizeAndType, alignment] = creature.meta.split(',')
  const [size, ...type] = sizeAndType.split(' ')

  return {
    size: size as CreatureSize,
    type: type.join(' ') as CreatureType,
    alignment: alignment?.trim(),
  }
}
export const type = (creature: Creature) => parseMeta(creature).type
export const shortTraits = (creature: Creature) =>
  pick(creature, [
    'Saving Throws',
    'Skills',
    'Damage Immunities',
    'Damage Resistances',
    'Damage Vulnerabilities',
    'Condition Immunities',
    'Senses',
    'Languages',
    'Challenge',
  ])
export const physicalTraits = (creature: Creature) =>
  pick(creature, ['Armor Class', 'Hit Points', 'Speed'])
