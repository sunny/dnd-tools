type CharacterClass =
  | 'Artificer'
  | 'Barbarian'
  | 'Bard'
  | 'Cleric'
  | 'Druid'
  | 'Fighter'
  | 'Monk'
  | 'Paladin'
  | 'Ranger'
  | 'Sorcerer'
  | 'Warlock'
  | 'Wizard'

export type SpellComponent = 'V' | 'S' | 'M'

export type School =
  | 'Abjuration'
  | 'Conjuration'
  | 'Divination'
  | 'Enchantment'
  | 'Evocation'
  | 'Illusion'
  | 'Transmutation'
  | 'Necromancy'

export type Spell = {
  castingTime: string
  class: CharacterClass[]
  components: SpellComponent[]
  concentration: boolean
  description: string
  duration: string
  higherLevel?: string | null
  level: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9
  material?: string
  name: string
  page?: string
  range: string
  ritual: boolean
  school: School
}
