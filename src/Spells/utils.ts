import { School, Spell } from './types'

export const CHARACTER_CLASSES = [
  'Bard',
  'Cleric',
  'Druid',
  'Paladin',
  'Ranger',
  'Sorcerer',
  'Warlock',
  'Wizard',
]

/**
 * inverse of `prettyLevel`, takes a string
 * and returns a number 0 — 9,
 * or `undefined` if a level can't be found.
 */
export const levelFromString = (
  levelStr: string
): Spell['level'] | undefined => {
  if (levelStr.toLowerCase().includes('cantrip')) return 0
  const result = levelStr.match(/\d/)?.[0]
  if (result == null) return undefined
  return Number(result) as Spell['level']
}

/**
 * takes a spell level number and
 * returns a string more suitable for display.
 */
export const prettyLevel = (level: number) => {
  if (level === 0) return 'cantrip'
  if (level === 1) return '1st level'
  if (level === 2) return '2nd level'
  if (level === 3) return '3rd level'
  return `${level}th level`
}

export const levelAndSchool = (level: number, school: School) => {
  const schoolStr = school.toLowerCase()
  if (level === 0) return `${schoolStr} cantrip`

  return `${prettyLevel(level)} ${school}`
}
