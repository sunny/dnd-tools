import compose from 'compose-function'
import cx from 'classnames'
import { range } from 'remeda'
import { FunctionalComponent as FC } from 'preact'
import { useState } from 'preact/hooks'

import styles from 'src/components/Create.module.scss'
import { getInputVal } from 'src/Creatures/utils'
import { useBool } from 'src/hooks'

import { Spell, SpellComponent } from './types'
import { CHARACTER_CLASSES, levelFromString, prettyLevel } from './utils'

const SCHOOLS = [
  'Abjuration',
  'Conjuration',
  'Divination',
  'Enchantment',
  'Evocation',
  'Illusion',
  'Transmutation',
  'Necromancy',
]

type Props = {
  onSave: (spell: Spell) => unknown
}

const CreateSpell: FC<Props> = ({ onSave }) => {
  const [castingTime, setCastingTime] = useState<Spell['castingTime']>('')
  const [classes, setClasses] = useState<Spell['class']>([])
  const [concentration, { toggle: toggleConcentration }] = useBool(false)
  const [description, setDescription] = useState<Spell['description']>('')
  const [higherLevel, setHigherLevel] = useState<Spell['higherLevel']>('')
  const [duration, setDuration] = useState<Spell['duration']>('')
  const [level, setLevel] = useState<Spell['level']>(0)
  const [name, setName] = useState<Spell['name']>('')
  const [school, setSchool] = useState<Spell['school']>('Abjuration')
  const [spellRange, setSpellRange] = useState<Spell['range']>('')
  const [ritual, { toggle: toggleRitual }] = useBool(false)
  // spell components
  const [verbal, { toggle: toggleVerbal }] = useBool(false)
  const [somatic, { toggle: toggleSomatic }] = useBool(false)
  const [material, { toggle: toggleMaterial }] = useBool(false)

  const save = (e: Event) => {
    e.preventDefault()

    // prettier-ignore
    const components = [
      verbal && 'V',
      somatic && 'S',
      material && 'M'
    ].filter(Boolean) as SpellComponent[]

    // TODO: validation
    onSave({
      castingTime,
      class: classes,
      components,
      concentration,
      description,
      duration,
      higherLevel,
      level,
      name,
      school,
      range: spellRange,
      ritual,
    })
  }

  const title = name || 'New Spell'
  return (
    <form class={cx(styles.container, styles.itemEditor)} onSubmit={save}>
      <h2>{title}</h2>

      <label for="name">name</label>
      <input
        class={styles.smallInput}
        id="name"
        onInput={compose(setName, getInputVal)}
        placeholder="Minor Telekinesis"
        value={name}
      />

      <label for="level">level</label>
      <select
        id="level"
        onChange={compose(setLevel, levelFromString, getInputVal)}
        value={prettyLevel(level)}
      >
        {range(0, 10).map((level) => (
          <option>{prettyLevel(level)}</option>
        ))}
      </select>

      <label for="classes">classes</label>
      <select
        id="classes"
        multiple
        onChange={compose(
          setClasses,
          (options: HTMLOptionElement[]) =>
            Array.from(options).map((option) => option.innerText),
          (e: Event) => (e.target! as HTMLSelectElement).selectedOptions
        )}
      >
        {CHARACTER_CLASSES.map((cc) => (
          <option>{cc}</option>
        ))}
      </select>

      <label for="school">school</label>
      <select
        id="school"
        onChange={compose(setSchool, getInputVal)}
        value={school}
      >
        {SCHOOLS.map((school) => (
          <option>{school}</option>
        ))}
      </select>

      <label for="casting-time">casting time</label>
      <input
        class={styles.smallInput}
        id="casting-time"
        onInput={compose(setCastingTime, getInputVal)}
        placeholder="1 action, 1 reaction, 10 minutes, etc"
        value={castingTime}
      />

      <label for="ritual">ritual</label>
      <input
        id="ritual"
        onChange={toggleRitual}
        type="checkbox"
        value={ritual.toString()}
      />

      <label for="range">range</label>
      <input
        class={styles.smallInput}
        id="range"
        onChange={compose(setSpellRange, getInputVal)}
        placeholder="Self, 120 ft., etc"
        type="text"
        value={spellRange}
      />

      <label for="concentration">concentration</label>
      <input
        id="concentration"
        onChange={toggleConcentration}
        type="checkbox"
        value={concentration.toString()}
      />

      <label for="duration">duration</label>
      <input
        class={styles.smallInput}
        id="duration"
        onChange={compose(setDuration, getInputVal)}
        placeholder="Instantaneous, 1 bonus action, etc"
        type="text"
        value={duration}
      />

      <fieldset class={styles.componentsContainer}>
        <legend>components</legend>

        <div class={styles.componentsInputs}>
          <label aria-label="verbal" for="verbal">
            V
          </label>
          <input id="verbal" onChange={toggleVerbal} type="checkbox" />

          <label aria-label="somatic" for="somatic">
            S
          </label>
          <input id="somatic" onChange={toggleSomatic} type="checkbox" />

          <label aria-label="material" for="material">
            M
          </label>
          <input id="material" onChange={toggleMaterial} type="checkbox" />
        </div>
      </fieldset>

      <label for="description">description</label>
      <textarea
        class={styles.description}
        id="description"
        onInput={compose(setDescription, getInputVal)}
        placeholder="description (markdown is supported)"
        value={description}
      />

      <label for="higher-levels">at higher levels</label>
      <textarea
        class={styles.description}
        id="higher-levels"
        onInput={compose(setHigherLevel, getInputVal)}
        placeholder="the damage increases by 1d6 for every level above 3rd"
        value={higherLevel ?? ''}
      />

      <button class={styles.saveButton} type="submit">
        save
      </button>
    </form>
  )
}

export default CreateSpell
