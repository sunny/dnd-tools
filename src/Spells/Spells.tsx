import useKeyBind from '@zanchi/use-key-bind'
import compose from 'compose-function'
import { useEffect, useRef, useState } from 'preact/hooks'
import { matchSorter } from 'match-sorter'

import { ListItem, SearchHeader, useExpandableList } from 'src/components'
import listStyles from 'src/components/ExpandableList/ExpandableList.module.scss'
import tabStyles from 'src/components/TabbedContainer.module.scss'
import { getInputVal } from 'src/Creatures/utils'
import { useBool, useJson } from 'src/hooks'

import CreateSpell from './CreateSpell'
import ExpandedSpell from './ExpandedSpell'
import { Spell } from './types'
import { levelAndSchool } from './utils'

const Spells = () => {
  const [creating, { toggle: toggleCreating }] = useBool(false)
  const [spells, { add: addSpell }] = useJson<Spell>('spells')
  const [searchTerm, setSearchTerm] = useState('')
  const containerRef = useRef<HTMLElement | null>(null)
  const hasFocus = () =>
    Boolean(containerRef.current?.contains(document.activeElement))

  const filteredSpells = matchSorter(spells, searchTerm, { keys: ['name'] })
  const [ExpandableList, { collapse, expand, expanded, select, selected }] =
    useExpandableList<Spell>()

  useKeyBind(
    ['Enter'],
    () => {
      if (!hasFocus()) return
      if (filteredSpells.length > 0) {
        // we're hitting enter from the input
        if (selected == null) {
          expand(0)
          select(0)
        } else if (selected === expanded) {
          collapse()
        } else {
          expand(selected)
        }
      }
    },
    []
  )

  useEffect(() => {
    collapse()
    select(null)
  }, [searchTerm])

  return (
    <section
      aria-labelledby="Spells-tab"
      class={tabStyles.tabPanel}
      ref={containerRef}
      role="tabpanel"
    >
      <SearchHeader
        onAdd={toggleCreating}
        onInput={compose(setSearchTerm, getInputVal)}
        searchTerm={searchTerm}
        title="Spells"
      />
      {creating && <CreateSpell onSave={addSpell} />}
      <ExpandableList
        className={listStyles.list}
        hasFocus={hasFocus}
        items={filteredSpells}
        ExpandedItem={ExpandedSpell}
        ListItem={({ onExpand, selected, ...spell }) => (
          <ListItem
            onClick={onExpand}
            subText={levelAndSchool(spell.level, spell.school)}
            selected={selected}
            title={spell.name}
            twoLine
          />
        )}
      />
    </section>
  )
}

export default Spells
