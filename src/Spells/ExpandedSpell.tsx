import { FunctionalComponent as FC } from 'preact'

import styles from 'src/components/ExpandedItem.module.scss'

import { Spell } from './types'
import { levelAndSchool } from './utils'

type Props = Spell & {
  onCollapse: () => unknown
}

const ExpandedSpell: FC<Props> = ({ onCollapse, ...spell }) => {
  return (
    <li class={styles.expandedItem}>
      <div class={styles.titleGroup} onClick={onCollapse}>
        <h2>{spell.name}</h2>
        <p class={styles.meta}>{levelAndSchool(spell.level, spell.school)}</p>
      </div>

      <dl>
        <div>
          <dt>Classes: </dt>
          <dd>
            <ul class={styles.inline}>
              {spell.class.map((c) => (
                <li>{c}</li>
              ))}
            </ul>
          </dd>
        </div>

        <div>
          <dt>Casting Time: </dt>
          <dd>{spell.castingTime}</dd>
        </div>

        <div>
          <dt>Range: </dt>
          <dd> {spell.range}</dd>
        </div>

        <div>
          <dt>Components: </dt>
          <dd>{spell.components.join(', ')}</dd>
        </div>

        <div>
          <dt>Duration: </dt>
          <dd>{spell.duration}</dd>
        </div>
      </dl>

      <p class={styles.description}>{spell.description}</p>
    </li>
  )
}

export default ExpandedSpell
