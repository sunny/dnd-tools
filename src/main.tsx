import { render } from 'preact'
// this should come before any JS import.
// otherwise, other CSS gets loaded first and
// the app's `@layer`s are declared in an incorrect order.
import './index.css'
import { App } from 'src/components'

render(<App />, document.getElementById('app')!)
